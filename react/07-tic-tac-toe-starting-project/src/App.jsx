import Player from "./components/Player.jsx";
import GameBoard from "./components/GameBoard.jsx";
import {useState} from "react";
import Log from "./components/Log.jsx";

function App() {
    const [gameTurn, setGameTurn] = useState([]);
    const [activePlayer, setAcivePlayer] = useState("X");

    function handleSelectSquare(rowIndex, colIndex) {
        setAcivePlayer((curActivePlayer) => curActivePlayer === "X" ? "O" : "X");
        setGameTurn(prevTurn => {
            //return [...prevGameTurn, [rowIndex, colIndex]];

            let currentPlayer = 'X'
            if (prevTurn.length > 0 && prevTurn[0].player === 'X') {
                currentPlayer = 'O'
            }

            const updatedGameTurn = [
                {square: {row: rowIndex, col: colIndex},
                    player: activePlayer},
                ...prevTurn];

            return updatedGameTurn;
        });
    }

  return (
      <main>
          <div id="game-container">
              <ol id="players" className="highlight-player">
                  <Player initialPlayerName="Player 1" symbol="X" isActive={activePlayer === 'X'}></Player>
                  <Player initialPlayerName="Player 2" symbol="O" isActive={activePlayer === 'O'}></Player>
              </ol>
              <GameBoard onSelectSquare={handleSelectSquare} turns={gameTurn} />
          </div>
          <Log/>
      </main>
  )
}

export default App
