import {useState} from "react";

export default function Player({initialPlayerName, symbol, isActive}) {
    const [palyerName, setPlayerName] = useState(initialPlayerName);
    const [isEditing, setIsEditing] = useState(false);

    function handleClick() {
        setIsEditing(editing =>!editing);
    }

    function handleNameChange(event) {
        setPlayerName(event.target.value);
    }

    let editablePlayerName = <span className="player-name">{palyerName}</span>;
    let btnCaption = 'Edit';

    if (isEditing) {
        editablePlayerName = <input type="text" value={palyerName} onChange={handleNameChange} />;
        btnCaption = 'Save';
    }
    return (
        <li className={isActive ? "active" : undefined}>
            <span className="player">
                {editablePlayerName}
                <span className="player-symbol">{symbol}</span>
            </span>
            <button onClick={handleClick}>{btnCaption}</button>
        </li>
    );

}