import {useState} from "react";

const initialGameBoard= [
    [null, null, null],
    [null, null, null],
    [null, null, null],
];

export default function  GameBoard({onSelectSquare, turns}) {
    let gameBoard = initialGameBoard;

    for (const turn of turns) {
        const {square, player} = turn;
        const {row, col} = square;
        gameBoard[row][col] = player;
    }
    /*const [gameBoard, setGameBoard] = useState(initialGameBoard);

    function handleSelectSquare(rowIndex, colIndex) {
        setGameBoard((preGameBoard) => {
            const updatedGameBoard = [...preGameBoard.map(innerArray => [...innerArray])];
            updatedGameBoard[rowIndex][colIndex] = activePlayerSymbol;
            return updatedGameBoard;
        });
        onSelectSquare();
    }*/

    return (<ol id="game-board">
        {gameBoard.map((row, rowIndex) => (<li key={rowIndex}>
          <ol>
                {row.map((palyerSymbol, cellIndex) => (<li key={cellIndex}>
                    <button onClick={() => onSelectSquare(rowIndex, cellIndex)}>{palyerSymbol}</button>
                </li>))}
            </ol>
        </li>)
        )}
    </ol>);
}